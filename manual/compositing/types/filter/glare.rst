.. index:: Compositor Nodes; Glare
.. _bpy.types.CompositorNodeGlare:

**********
Glare Node
**********

.. figure:: /images/compositing_node-types_CompositorNodeGlare.webp
   :align: right
   :alt: Glare Node.

The *Glare node* is used to add lens flares, fog,
glows around bright parts of an image.


Inputs
======

Image
   Standard color input.


Properties
==========

Glare Type
   :Bloom:
      Simulates the glow around bright objects caused by light scattering in eyes and cameras.

      Size
         Scale of the glow relative to the size of the image. 9 means the glow can cover the
         entire image, 8 means it can only cover half the image, 7 means it can only cover quarter
         of the image, and so on.
   :Ghosts:
      Creates a haze over the image.
   :Streaks:
      Creates bright streaks used to simulate lens flares.

      Streaks
         Total number of streaks.
      Angle Offset
         The rotation offset factor of the streaks.
      Fade
         Fade out factor for the streaks.
   :Fog Glow:
      Simulates the glow around bright objects caused by light scattering in eyes and cameras.
      This is similar to the *Bloom* mode, but is more physically accurate, at the cost of much
      slower computation time.

      Size
         Scale of the glow relative to the size of the image. 9 means the glow will cover the
         entire image, 8 means it will cover half the image, 7 means it will cover quarter of the
         image, and so on.
   :Simple Star:
      Works similar to *Streaks* but gives a simpler shape looking like a star.

      Fade
         Fade out factor for the streaks.
      Rotate 45
         Rotate the streaks by 45°.

Quality
   If not set to something other the *High*,
   then the glare effect will only be applied to a low resolution copy of the image.
   This can be helpful to save render times while only doing preview renders.

Iterations
   The number of times to run through the filter algorithm.
   Higher values will give more accurate results but will take longer to compute.
   Note that, this is not available for *Fog Glow* as it does not use an iterative-based algorithm.

Color Modulation
   Used for *Streaks* and *Ghosts* to create a special dispersion effect.

   Johannes Itten describes this effect, Color Modulation, as subtle variations in tones and chroma.

Mix
   Value to control how much of the effect is added on to the image.
   A value of -1 would give just the original image, 0 gives a 50/50 mix, and 1 gives just the effect.

Threshold
   Pixels brighter than this value will be affected by the glare filter.


Outputs
=======

Image
   Standard color output.
