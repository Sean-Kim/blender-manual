.. index:: Compositor Nodes; Pixelate
.. _bpy.types.CompositorNodePixelate:

*************
Pixelate Node
*************

.. figure:: /images/compositing_node-types_CompositorNodePixelate.webp
   :align: right
   :alt: Pixelate Node.

The Pixelate node reduces the detail in an image by making individual pixels more prominent.
It results in a blocky or mosaic-like appearance, where the fine details of the image are obscured,
and the overall image is represented using larger, more noticeable pixels.


Inputs
======

Color
   Standard color input.


Properties
==========

Pixel Size
   The size of the pixels in the output image, measured in pixels.


Outputs
=======

Color
   Standard color output.


Example
=======

.. figure:: /images/compositing_types_filter_pixelate_example.png
