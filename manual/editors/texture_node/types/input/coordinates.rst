.. _bpy.types.TextureNodeCoordinates:

****************
Coordinates Node
****************

.. figure:: /images/node-types_TextureNodeCoordinates.webp
   :align: right
   :alt: Coordinates node.


Inputs
======

This node has no inputs.


Properties
==========

This node has no properties.


Outputs
=======

Coordinates
   The Coordinates node outputs the local geometry coordinates,
   relative to the bounding box.