.. _bpy.types.Modifier:

#############
  Modifiers
#############

.. toctree::
   :maxdepth: 2

   introduction.rst
   common_options.rst


Built-In Modifiers
==================

.. toctree::
   :maxdepth: 2

   modify/index.rst
   generate/index.rst
   deform/index.rst
   normals/index.rst
   physics/index.rst
