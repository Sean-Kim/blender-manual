.. index:: Geometry Nodes; Project Point
.. _bpy.types.FunctionNodeProjectPoint:

******************
Project Point Node
******************

.. figure:: /images/node-types_FunctionNodeProjectPoint.webp
   :align: right
   :alt: Project Point node.

The *Project Point* node projects a position vector using a :term:`Transformation Matrix`,
using location, rotation, scale, and perspective divide.


Inputs
======

Vector
   The position vector vector to transform.
Transformation
   The transformation matrix.


Properties
==========

This node has no properties.


Outputs
=======

Vector
   The projected position vector.
