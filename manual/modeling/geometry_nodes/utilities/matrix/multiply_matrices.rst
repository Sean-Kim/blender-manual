.. index:: Geometry Nodes; Multiply Matrices
.. _bpy.types.FunctionNodeMatrixMultiply:

**********************
Multiply Matrices Node
**********************

.. figure:: /images/node-types_FunctionNodeMatrixMultiply.webp
   :align: right
   :alt: Multiply Matrices node.

The *Multiply Matrices* node performs a
`matrix multiplication <https://en.wikipedia.org/wiki/Matrix_multiplication>`__
on two input matrices.


Inputs
======

Matrix
   The first multiplication.
Matrix
   The second multiplication.


Properties
==========

This node has no properties.


Outputs
=======

Matrix
   The resulting matrix multiplication.
