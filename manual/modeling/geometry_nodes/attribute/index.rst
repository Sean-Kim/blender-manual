
###################
  Attribute Nodes
###################

Nodes for working with data stored per object element, e.g. vertex groups.

.. toctree::
   :maxdepth: 1

   attribute_statistic.rst
   domain_size.rst

-----

.. toctree::
   :maxdepth: 1

   blur_attribute.rst
   capture_attribute.rst
   remove_named_attribute.rst
   store_named_attribute.rst
