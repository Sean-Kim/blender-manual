
******************
Mask Gesture Tools
******************

.. reference::

   :Mode:      Sculpt Mode

Mask gesture tools apply a constant value to all selected vertices within the selection area.
By default, these tools fully mask each vertex. Holding :kbd:`Ctrl` while performing the selection
clears the mask.

All mask gesture tools can be activated in the Toolbar and are comprised of the following:

.. _tool-box-mask:

Box Mask
========

.. reference::

   :Mode:      Sculpt Mode
   :Tool:      :menuselection:`Toolbar --> Box Mask`
   :Shortcut:  :kbd:`B`


Creates a new :doc:`Mask </sculpt_paint/sculpting/editing/mask>` based on a
:ref:`box gesture <gesture-tool-box>`.

.. _tool-lasso-mask:

Lasso Mask
==========

.. reference::

   :Mode:      Sculpt Mode
   :Tool:      :menuselection:`Toolbar --> Lasso Mask`

Creates a new :doc:`Mask </sculpt_paint/sculpting/editing/mask>` based on a
:ref:`lasso gesture <gesture-tool-lasso>`.

.. _tool-line-mask:

Line Mask
=========

.. reference::

   :Mode:      Sculpt Mode
   :Tool:      :menuselection:`Toolbar --> Line Mask`

Creates a new :doc:`Mask </sculpt_paint/sculpting/editing/mask>` based on a
:ref:`line gesture <gesture-tool-line>`.

.. _tool-polyline-mask:

Polyline Mask
=============

.. reference::

   :Mode:      Sculpt Mode
   :Tool:      :menuselection:`Toolbar --> Polyline Mask`

Creates a new :doc:`Mask </sculpt_paint/sculpting/editing/mask>` based on a
:ref:`polyline gesture <gesture-tool-polyline>`.

Tool Settings
=============

Front Faces Only
   Only creates a mask on the faces that face towards the view.