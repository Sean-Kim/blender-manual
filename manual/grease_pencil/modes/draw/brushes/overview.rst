
********
Overview
********

There are a number of brushes for draw mode bundled in the *Essentials* asset library. This is an
overview of all of them.

:doc:`Draw <draw>`
   Draw free-hand strokes.

:doc:`Fill <fill>`
   Automatic fill closed strokes areas.

:doc:`Erase <erase>`
   Erase strokes.

:doc:`Tint <tint>`
   Colorize stroke points.
